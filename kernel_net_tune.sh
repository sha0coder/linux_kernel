#!/bin/bash


#Agilizar los CLOSE_WAIT que es realmente el problema
echo 15 >  /proc/sys/net/ipv4/netfilter/ip_conntrack_tcp_timeout_close_wait 


#Finalizacion de conexiones mas rapidas
echo 15 > /proc/sys/net/ipv4/tcp_fin_timeout

#Tcp keepalive mas corto
echo 15 > /proc/sys/net/ipv4/tcp_keepalive_intvl

#keepalive timeout
echo 5 > /proc/sys/net/ipv4/tcp_keepalive_probes

#reciclar time_waits
echo 1 > /proc/sys/net/ipv4/tcp_tw_recycle

#Reusar sockets para ahorrar descriptores
echo 1 > /proc/sys/net/ipv4/tcp_tw_reuse


#/LINUX-SOURCE-DIR/include/linux/skbuff.h
#Look for SK_WMEM_MAX & SK_RMEM_MAX

#/LINUX-SOURCE-DIR/include/net/tcp.h
#Look for MAX_WINDOW & MIN_WINDOW 

